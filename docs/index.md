# Accueil

L’objectif principal de ce site de documentation est de présenter les différents aspects techniques nécessaires à la réalisation du [projet BA2 biomed 2023-2024](https://ulb-polytech.gitlab.io/ba2-projects/biomed/2023-2024/enonce/), tout en renvoyant vers des tutoriels existants, en mettant en avant des fonctionnalités intéressantes par le biais de captures d’écran et en mentionnant des commandes de base utiles pour le projet. 

Nous y aborderons également l’installation des outils et leur utilisation. Sauf mention contraire, toute installation se fera avec les options de base de l'installateur, donc juste en cliquant _Next_ dans la plupart des cas.

Notons qu'une partie de la documentation a été reprise des projets BA2 2021-2022 de [Tomographie Infrarouge](https://lisa.polytech.ulb.be/en/lisa-teaching/ba2-biomedical-projects/tomographie-infrarouge) et 2022-2023 d'[Analyse de mouvements avec une caméra 3D](https://lisa.polytech.ulb.be/en/lisa-teaching/ba2-biomedical-projects/analyse-de-mouvements-avec-une-camera-3d), ce qui explique que vous trouverez plusieurs captures d'écran qui n'ont pas été refaites spécifiquement pour ce projet, même si le texte a été majoritairement adapté.


## Plan

1. Installation, configuration et utilisation de [Python avec Anaconda](anaconda.md) (et Jupyter).
2. Utilisation de [Gitlab et Git](gitlab.md) incluant, entre autre, une [vidéo](gitlab.md#utilisation-de-git-et-gitlab-video).
3. Installation, configuration et utilisation de l'environnement de développement (_IDE_) [_PyCharm_](pycharm.md).


## Remarque sur les références

Les sources sont citées de manière non conventionnelles en donnant juste des liens car ce site n'est pas un rapport scientifique, juste de la documentation ! Ce genre de référencement très succinct ne sera donc pas accepté dans vos rapports.


**Dernière révision** : {{ git_revision_date }}

