# Gitlab et Git

## Introduction

L'objectif de cette page est de vous donner des informations pour l'installation et l'utilisation de _gitlab/git_ dans le cadre du projet BA2 de cette année.

[GitLab](https://gitlab.com) est un logiciel libre basé sur _git_ proposant des fonctionnalités wiki, un suivi des bugs et l’intégration/livraison continue (CI/CD - Continuous Integration/Continuous Deployment) ([source Wikipedia](https://fr.wikipedia.org/wiki/GitLab)).

Git est un logiciel de gestion de versions, c.à.d. 

> _un logiciel qui permet de stocker un ensemble de fichiers en conservant la chronologie de toutes les modifications qui ont été effectuées dessus ([source Wikipedia](https://fr.wikipedia.org/wiki/Logiciel_de_gestion_de_versions))_


## Installation et configuration Git

La procédure d’installation est décrite, par exemple, sur ce [site](https://git-scm.com/book/fr/v2/D%C3%A9marrage-rapide-Installation-de-Git).

Pour Windows, utilisez la version avec le setup téléchargeable sur [le site de git](https://git-scm.com/download/win).
Nous conseillons d’installer et d’utiliser [Notepad++](https://notepad-plus-plus.org/downloads/) comme éditeur par défaut.
Toutes les autres options d’installation peuvent être laissées telles quelles.

Pour Ubuntu, il suffit d'utiliser le gestionnaire de "packages" pour installer _git_ ou avec la commande suivante dans un _terminal_ `sudo apt-get install git`.

Un tutoriel de base est donné à [l'adresse de Git](https://git-scm.com/docs/gittutorial).
Dans un “command prompt” (`cmd` sous windows) ou un “Anaconda prompt” ou un terminal (Linux/Mac), la commande `git` devrait être alors accessible. Il est également possible, sous Windows, de cliquer sur un dossier avec le bouton droit pour ouvrir un terminal “similaire à linux” avec “Git Bash Here”. L’alternative est d’utiliser le “Git GUI Here” pour obtenir une interface graphique git (voir [la vidéo d'utilisation de Git et Gitlab](#utilisation-de-git-et-gitlab-video)). 

Préalablement, veillez à avoir configuré votre git local avec (à faire une seule fois et de préférence avec l'adresse e-mail fournie dans gitlab) :
```
git config --global user.name "Votre nom"
git config --global user.email votreemail@domaine.com
```

Pour commencer, nous allons donner un petit exemple simple d’utilisation pour un dossier contenant des fichiers python et l’”Anaconda Prompt” (voir [Anaconda](anaconda.md#console-anaconda)) : 

Voici la suite des commandes : 
```
cd test                         #aller dans le répertoire test
git init                        #initialiser le répertoire pour le suivi par git
git add *.py                    #on ajoute tous les fichiers se terminant par py c.à.d. les fichiers pythons
git status                      #permet de voir le statut des fichiers 
git commit -am “Initial commit” #on ajoute une nouvelle version des fichiers au répositoire local; un message au commit doit toujours être présent; le -m permet de le préciser directement entre “” sinon il vous sera demandé de le mentionner, le “a” avant permet d’ajouter les fichiers modifiés au commit (cela remplace le git add pour un fichier déjà suivi).
```


## Gitlab

Les commandes précédentes sont intéressante pour un suivi local mais nous conseillons d’utiliser [Gitlab](https://gitlab.com) afin de collaborer et de partager votre code avec d’autres utilisateurs.

A partir de Gitlab, il est possible de directement créer un projet avec un suivi de version _git_. Dans ce document, nous allons vous montrer la méthode par défaut qui consiste à créer un nouveau projet (vide) et celle qui consiste à simplement faire un _fork_ d'un projet existant.

Il est important de souligner que gitlab a procédé récemment à une refonte de son interface web. Les captures d'écrans ci-dessous ne présentent donc plus exactement la même interface même si tous les éléments important et utilisé dans cette documentation n'ont pas changé de place !

### Créer un nouveau projet

Dans votre interface par défaut, comme le montre la figure suivante, il suffit de cliquer sur le bouton "_New Project_".

![Nouveau projet Gitlab](img/gitlab_newproject.png)

Ensuite, il suffit de sélectionner "_Create blank project_".

![Initiate blank project Gitlab](img/gitlab_blankproject.png)

Dans ce _blank project_, comme le montre la figure suivante, vous devrez :

- Assigner un nom de projet dans le champ "_Project name_".
- Attribuer éventuellement un groupe ou un sous-groupe avec le "_Project URL_".
- Optionnellement modifier le "_Project slug_".
- En option, donner une description du projet dans "_Project description_".
- Attribuer un niveau de visibilité qui ne peut pas être "plus visible" que celui du groupe auquel le projet appartient :
    - _Private_ pour un projet/répositoire accessible qu'aux membres du projet.
    - _Internal_ pour un projet à tous les membres inscrits au gitlab.
    - _Public_ pour un projet accessible au monde entier.
- Initialiser le répositoire avec un fichier `README.md`. En pratique, nous conseillons de **décocher cette option** afin de pouvoir mettre plus facilement un projet/dossier déjà existant.

![Create blank project Gitlab](img/gitlab_blankprojectnext.png)

**NB:** Comme vous pouvez le remarquer sur la figure ci-dessus, le niveau  de visibilité "Public" du projet ne peut être sélectionné car il appartient à votre groupe "BIOMEDx" (x=1 à 5) qui a une visibilité "_Private_". 

Après avoir cliqué sur le bouton "_Create project_", votre projet vide est donc créé et la figure suivante vous donne les différentes options pour ajouter des fichiers.

![Gitlab blank projet finish](img/gitlab_blankprojectfinish.png)

Nous reprenons donc ci-dessous les différentes options et commandes proposées pour ajouter vos fichiers avec _git_.

Tout d'abord le configuration globale de votre git, comme déjà expliqué dans [l'installation de git](#installation-et-configuration-git), par exemple pour "Rudy"
```
git config --global user.name "Rudy Ercek"
git config --global user.email "rudy.ercek@ulb.be"
```

Les commandes suivantes dépendront ensuite de l'existance ou nom de fichiers.

Pour créer un nouveau repositoire local avec un fichier README.md, vous devez taper la suite des commandes suivantes dans un _shell_ : 
```
git clone https://gitlab.com/.../monprojet.git
cd monprojet
git switch -c main
touch README.md     #commande linux, en windows, créer un fichier README.md avec notepad ou autre programme.
git add README.md
git commit -m "add README"
git push -u origin main
```

De manière similaire, pour ajouter un dossier complet avec des fichiers existants localement, la liste des commandes est la suivante : 
```
cd existing_folder
git init --initial-branch=main
git remote add origin https://gitlab.com/.../monprojet.git
git add .
git commit -m "Initial commit"
git push -u origin main
```

Et pour ajouter un répositoire _git_ existant :
```
cd existing_repo
git remote rename origin old-origin
git remote add origin https://gitlab.com/.../monprojet.git
git push -u origin --all
git push -u origin --tags
```

### _Fork_ d'un projet existant

Sur un serveur Gitlab, vous pouvez facilement dupliquer un projet existant afin de créer un nouveau projet sur ce même Gitlab sans affecter le projet original. Pour ce faire, il suffit de faire un _fork_ de ce projet. Il existe alors une relation entre le projet d'origine et le nouveau projet qui permet de répercuter des modifications du projet d'origine vers les _fork_ de ce projet ou l'inverse.

Par exemple pour ce projet, un [template de projet nommé IRCam](https://gitlab.com/ulb-polytech/ba2-projects/biomed/2023-2024/ircam) a été créé pour vous aider. Pour créer un _Fork_ de celui-ci, il suffit de cliquer sur le [bouton](https://gitlab.com/ulb-polytech/ba2-projects/biomed/2023-2024/ircam/-/forks/new) de la figure suivante.

![Fork project](img/gitlab_fork.png)

Ensuite, de manière similaire au nouveau projet (vide) et comme le montre la figure suivante, vous devrez : 

- Eventuellement modifier le nom de projet avec un autre dans le champ "_Project name_" .
- Attribuer un groupe ou un sous-groupe avec le "_Project URL_", dans votre cas, _.../BIOMEDx_ ou si vous voulez le tester personnellement, dans votre espace privé d'utilisateur (i.e. votre _gitlab username_).
- Optionnellement modifier le "_Project slug_".
- Modifier éventuellement la description du projet d'origine dans le champ "_Project description_".
- Attribuer un niveau de visibilité qui ne peut pas être "plus visible" que celui du groupe auquel le projet appartient :
    - _Private_ pour un projet/répositoire accessible qu'aux membres du projet.
    - _Internal_ pour un projet à tous les membres inscrits au Gitlab.
    - _Public_ pour un projet accessible au monde entier.

![Fork next](img/gitlab_forknext.png)

Finalement, vous obtiendrez alors une duplication du projet d'origine dans votre groupe _.../BIOMEDx_ ou dans votre espace privé d'utilisateur. 

![Fork finished](img/gitlab_forkfinish.png)

### Projet _Forké_ mis-à-jour

Si le projet d'origine du _Fork_, donc le template du projet [IRCam](https://gitlab.com/ulb-polytech/ba2-projects/biomed/2022-2023/ircam), a été modifié, il est possible de répercuter ces changements sur un projet dupliqué "monprojet" en effectuant les commandes suivantes dans un _shell_.

```
git clone https://gitlab.com/.../monprojet.git
cd monprojet
git remote add upstream https://gitlab.com/ulb-polytech/ba2-projects/biomed/2023-2024/ircam.git
git pull upstream main
git push
```

Notons que si des fichiers identiques du template du projet et de "monprojet" ont été modifiés de chaque côté, il conviendra de résoudre les conflits en modifiant manuellement les fichiers problématiques dont vous pouvez avoir un aperçu avec la commande `git diff`.

## Utilisation de Git et Gitlab (vidéo)

Dans la vidéo suivante, l'utilisation de Git et Gitlab est expliquée par [Adrien Foucart](mailto:adrien.foucart@ulb.be) sur base du template (similaire à _IRCam_) du projet de tomographie.

![type:video](https://www.youtube.com/embed/tcFm03kUDZI)


## Publication de sites Web

A partir de Gitlab, il est possible de publier un site web statique (pages "_html_ + _javascript_") grâce à l'utilisation de [Gitlab Pages](https://docs.gitlab.com/ee/user/project/pages/).

Sans entrer dans les détails, Gitlab utilise les outils CI/CD afin de publier, par défaut, vos sites webs sur un "_wildcard_" domaine. 
Le "_wildcard_" domaine (*.domain) utilisé par [gitlab](https://gitlab.com) est le suivant "*.gitlab.io".

Les adresses des sites générés dépendront de son _namespace_ c.à.d. si le projet appartient à un (sous-)groupe ou à un utilisateur. Par exemple, pour un projet "monprojet" appartenant à l'utilisateur "rudy", le site généré sur [gitlab.com](https://gitlab.com) serait disponible à l'adresse ["https://rudy.gitlab.io/monprojet"](https://rudy.gitlab.io/monprojet). Pour un projet "ircam" appartenant au sous-groupe "BIOMED1", le site généré sur le [groupe gitlab des projets BA2 Biomed](https://gitlab.com/ulb-polytech/ba2-projects/biomed/2023-2024/) serait disponible à l'adresse ["https://ulb-polytech.gitlab.io/ba2-projects/biomed/2023-2024/biomed1/ircam"](https://ulb-polytech.gitlab.io/ba2-projects/biomed/2023-2024/biomed1/ircam). L'énoncé du projet biomed 2023-2024 est par exemple accessible à l'adresse ["https://ulb-polytech.gitlab.io/ba2-projects/biomed/2023-2024/enonce"](https://ulb-polytech.gitlab.io/ba2-projects/biomed/2023-2024/enonce) car il est associé au [projet "ENONCE"](https://gitlab.com/ulb-polytech/ba2-projects/biomed/2023-2024/enonce) du sous-groupe "BA2-Projects\BIOMED\2023-2024" du groupe principal (namespace) "ULB-Polytech".

Le [template de projet BA2](https://gitlab.com/ulb-polytech/ba2-projects/biomed/2023-2024/ircam) intègre cette fonctionnalité. Dès que des fichiers ["MarkDown"](https://ulb-polytech.gitlab.io/ba2-projects/biomed/2023-2024/ircam/) ou ["Jupyter Notebook" python](https://ulb-polytech.gitlab.io/ba2-projects/biomed/2023-2024/ircam/processing/) sont modifiés dans le sous-dossier [`www`](https://gitlab.com/ulb-polytech/ba2-projects/biomed/2023-2024/ircam/-/tree/main/www), le processus de publication par CI/CD est exécuté en utilisant ["MkDocs"](https://www.mkdocs.org) et le site est alors déployé à l'adresse ["https://ulb-polytech.gitlab.io/ba2-projects/biomed/2023-2024/ircam"](https://ulb-polytech.gitlab.io/ba2-projects/biomed/2023-2024/ircam). Afin de voir les erreurs éventuelles, il suffit de se rendre dans le menu ["CI/CD" --> "Pipelines"](https://gitlab.com/ulb-polytech/ba2-projects/biomed/2023-2024/ircam/-/pipelines) du projet comme le montre la figure ci-dessous.

![CI/CD for pages](img/gitlab_pages_cicd.png)

En cliquant sur un ["passed"](https://gitlab.com/ulb-polytech/ba2-projects/biomed/2023-2024/ircam/-/pipelines/971992984), la figure suivante donne les informations relatives à ce "pipeline".

![Pipeline for pages](img/gitlab_pages_pipeline.png)

Et en cliquant sur ["pages"](https://gitlab.com/ulb-polytech/ba2-projects/biomed/2023-2024/ircam/-/jobs/4899018435), l'ensemble des étapes d'exécution du "générateur MkDocs" peut être analysée. S'il devait avoir une erreur, généralement un fichier manquant, il est possible de retrouver l'information dans cette sortie "bash".

![Bash for pages](img/gitlab_pages_bash.png)

En cliquant sur le bouton ["_Download_"](https://gitlab.com/ulb-polytech/ba2-projects/biomed/2023-2024/ircam/-/jobs/4899018435/artifacts/download) de la figure précédente, il est possible de télécharger l'ensemble des _pages_ html générées (appelé "Artifacts") dans un fichier _zip_. 


## Sauvegarde d'un projet Gitlab

Même si une perte de données sur le serveur [gitlab.com](https://gitlab.com) est très peu probable, il est intéressant de pouvoir sauvegarder son projet. 

Même si un `git clone` ou un `git pull` permet de sauvegarder entièrement le répositoire, c.à.d. les codes et la documentation, une partie des données "Gitlab" ne le sont pas, par exemple, les _Issues_.

Afin de sauvegarder quasi toutes les données d'un projet Gitlab, un _export_ Gitlab du projet peut être effectué.

Comme le montre la figure suivante, pour générer cette sauvegarde, les différentes étapes sont les suivantes :

1. Allez dans le menu "Settings" --> "General" de votre projet.
2. Descendez tout en bas au niveau d'"Advanced" et cliquez sur le bouton "Expand" (qui devient alors "Collapse").
3. Cliquez finalement sur le bouton "Export project", un fichier sera généré après quelques minutes et un lien de téléchargement sera envoyé par mail et sera également disponible sur cette même page avec un bouton "Download Export".

![Export Project](img/gitlab_export_project.png)

Une fois la sauvegarde récupérée, il est possible alors de la restaurer sur un serveur gitlab en réalisant un "import" de manière similaire à la [création d'un nouveau projet](#creer-un-nouveau-projet). Nous ne détaillerons pas cette procédure qui est relativement simple.


## Conclusions et références

Git/Gitlab sont des outils très complets qui possèdent des fonctionnalités très avancées qui ont été survolées dans ce document. Même si l'utilisation de branches est conseillée pour minimiser le risque de conflits et avoir un code toujours fonctionnel sur la branche _main_, elle n'est pas obligatoire. 

Notons que, pour les nouveaux projets, la branche _main_ est actuellement utilisée par défaut tandis que la branche _master_ l'était précédemment, c'est pour cette raison que vous trouverez beaucoup de projets sur internet avec ce nom de branche principale.

Pour plus d’informations sur _git_, n’hésitez pas à vous renseigner sur internet. Par exemple, nous avons survolé ce tutoriel qui nous paraît très complet et bien rédigé [OpenClassRooms](https://openclassrooms.com/fr/courses/1233741-gerez-vos-codes-source-avec-git). Malheureusement, il ne semble plus être disponible sans inscription. Un autre tutoriel en anglais qui semble intéressant est [celui-ci](https://www.tutorialspoint.com/gitlab/index.htm).


**Dernière révision** : {{ git_revision_date }}
