# Environnement de développement _PyCharm_

## Présentation

[_PyCharm_](https://www.jetbrains.com/fr-fr/pycharm/) est un [IDE](https://www.redhat.com/fr/topics/middleware/what-is-ide) c.à.d. "un environnement de développement intégré" spécialement conçu pour _Python_.

Même si l'utilisation de _PyCharm_ n'est pas obligatoire pour votre projet, nous vous conseillons de l'installer et de l'utiliser car il dispose de fonctions intéressantes de [_debugging_](#execution-et-debugging-avec-pycharm) et permet de gérer des environnements virtuels, entre autre, avec [anaconda](anaconda.md) ainsi que le _versionning_ des fichiers avec [_git_](gitlab.md).

Dans la suite de ce chapitre, nous allons présenter l'installation et la configuration de _PyCharm_ en utilisant des captures réalisées avec le projet de l'année passée. Vous pouvez bien entendu l'adapter en utilisant le [template du projet 2023-2024](https://gitlab.com/ulb-polytech/ba2-projects/biomed/2023-2024/ircam) qui a été préalablement "cloné" dans un répertoire de l'ordinateur avec la commande `git clone https://gitlab.com/ulb-polytech/ba2-projects/biomed/2023-2024/ircam.git`. En effet, ce projet contient également du code _Python_ pouvant donc être exécuté dans _PyCharm_ !


## Installation

Sous Windows, il suffit de télécharger la version _community_ avec ce [lien direct](https://download-cdn.jetbrains.com/python/pycharm-community-2023.2.exe) pour la version 2023.2 ou ce [lien](https://www.jetbrains.com/fr-fr/pycharm/download/#section=windows) pour la dernière version disponible. Ensuite, il suffit d'exécuter le fichier et continuer l'installation par défaut en modifiant à votre guise certaines options (par exemple, raccourci sur le bureau, ...). A la fin de l'installation, le programme _PyCharm_ sera accessible dans vos applications du "Menu Démarrer".

Sous Ubuntu, vous pouvez télécharger la version 2023.2 _community_ avec ce [lien direct](https://download-cdn.jetbrains.com/python/pycharm-community-2023.2-aarch64.tar.gz). Pour l'installer, vous devriez déplacer le fichier dans votre "Dossier personnel" (_HOME_) et ensuite tapper la commande `sudo tar xzf pycharm-*.tar.gz -C /opt/` dans un terminal afin de décompresser l'application dans le dossier `/opt` de votre PC. Ensuite, pour lancer _PyCharm_, vous devez exécuter les deux commandes suivantes dans un terminal :
````shell
cd /opt/pycharm-*/bin
sh pycharm.sh
````
Une fois _PyCharm_ lancé avec un projet ouvert, nous conseillons de rajouter un raccourci à votre bureau Ubuntu en sélectionnant "Create Desktop Entry..." du menu "Tools". 


## Configuration et utilisation

### Premier démarrage

Lors de la première exécution de _PyCharm_, le programme affichera la fenêtre suivante

![Première exécution _PyCharm_](img/pycharm1.png)

En cliquant sur le bouton _Open_ de cette fenêtre, sélectionnez le répertoire contenant votre code source python dans l'arborescence et confirmer que vous faites confiance à ce projet avec le bouton "_Trust Project_" (éventuellement cocher la case "_Trust projects in ..._" si vous avez l'intention d'utiliser le répertoire racine de ce projet pour d'autres projets _Python_) comme le montre l'image suivante avec le projet de l'année passée "_kneemotion_".

![Confiance au projet](img/pycharm2.png)


### Création d'intepréteurs/environnements virtuels dans _PyCharm_

 Après cette opération, _PyCharm_ se lance et tente de détecter un interpréteur par défaut (c.à.d. un python), cela peut prendre un certain temps. En pratique, nous allons ajouter un interpréteur avec un environnement _conda_ en cliquant à l'endroit de la flèche [A] de la figure suivante.

![Projet ouvert](img/pycharm3.png)

Le menu "Interpréteur" de la figure suivante apparait alors avec diverses options. 

![Menu Python Interpreter](img/pycharm9.png)

Les annotations entre crochets [] de la figure ci-dessus correspondent respectivement à :

1. L'interpréteur/environnement actif pour le projet.
2. La configuration des interpréteurs/environnements permettant [d'ajouter ou supprimer des librairies](#ajout-de-librairies-a-un-environnement).
3. L'ajout d'un nouvel interpréteur.
4. La liste des interpréteurs disponibles permettant de sélectionner celui actif pour le projet cf. [1].

**Même si dans le cadre du projet BA2 2023-2024, l'utilisation d'environnement virtuel n'est pas nécessaire, nous vous invitons à lire la suite de cette section utilisant, comme base, le projet de l'année passée. Si vous ne le souhaitez pas, vous pouvez directement vous rendre à la section intitulée ["Terminal _Pycharm_"](#terminal-pycharm).**

#### Nouvel environnement virtuel
 
Nous allons ensuite créer un nouvel interpréteur en utilisant un environnement virtuel _conda_, donc en sélectionnant [3] de la figure précédente, la fenêtre suivante apparait alors. 

![Add Python Interpreter](img/pycharm4.png)

Les annotations de cette figure correspondent respectivement au :

1. Choix du type d'environnement virtuel qui sera toujours "_Conda Environment_" dans le cadre de ce projet même si vous êtes libre d'en sélectionner un autre.
2. Nom de l'interpréteur, par défaut, "New Virtualenv" lorsque vous en créez un nouveau. Il est possible de sélectionner un interpréteur existant comme nous le verrons [plus loin](#ajouter-un-environnement-existant).
3. Dossier de l'environnement virtuel où tous les fichiers seront installés (python + librairies). Ce champ sera automatiquement généré avec le nom du projet ouvert.
4. Version de python utilisée pour cet environnement, qui sera toujours 3.9 dans ce projet.
5. Chemin d'accès de l'exécutable "conda" qui devrait être automatiquement détecté par _PyCharm_, donc à ne pas modifier.
6. Nous conseillons de toujours cocher cette case pour rendre l'environnement créé accessible à d'autres projets _Python_ dans _PyCharm_.

Après avoir créé le nouvel environnement avec les options de la figure précédente (i.e. après avoir cliqué sur le bouton _OK_), _PyCharm_ doit indexer l'ensemble des librairies, ce qui peut prendre un certain temps. La figure suivante montre le processus en cours d'indexation avec la flèche [1] (_Updating skeletons_) pour le nouvel environnement _Python 3.9 (kneemotion)_ pointé par la flèche [2].

![Indexation de l'interpréteur](img/pycharm5.png)

Une fois cette indexation terminée, il est possible d'exécuter un fichier python, par exemple, en le sélectionnant avec le bouton droit et en cliquant sur _Run "..."_ comme le montre la figure suivante.

![Exécution d'un programme](img/pycharm6.png)

#### Ajouter un environnement existant

La figure suivante montre l'ajout d'un environnement _Anaconda_ existant dans _PyCharm_ où nous avons repris la même numérotation que pour le nouvel environnement. Pour ce faire, il suffit de sélectionner le fichier `python.exe` qui se trouve dans le dossier de l'environnement.

![Exécution d'un programme](img/pycharm8.png)

Pour connaître les chemins des environnements existants, il suffit de faire un `conda env list` dans un _shell_. La figure suivante montre un exemple de cette commande mais également l'activation d'un environnement créé dans _PyCharm_ en spécifiant son chemin d'accès vu qu'il n'a pas de nom propre associé à cet environnement.

![Liste des environnements anaconda](img/condaenv.png)

#### Ajout de librairies à un environnement

Après avoir cliqué sur le bouton [2] du menu "Interpréteur", la figure suivante apparait alors avec l'ensemble des librairies dans l'environnement courant.

![Liste des librairies installées](img/pycharm10.png)

Les différentes annotations numérotées de cette figure correspondent respectivement à

1. L'interpréteur (environnement virtuel) auquel les librairies peuvent être ajoutées ou supprimées.
2. La liste des librairies installées.
3. Au bouton "+" permettant d'ajouter une nouvelle libraire. Le bouton "-" à côté permet de supprimer une librairie sélectionnée dans la liste.

Après avoir cliqué sur le bouton "+" [3], la figure suivante est alors affichée.

![Liste des librairies disponibles](img/pycharm11.png)

Les annotations numérotées sur cette figure permettent respectivement de

1. Rechercher une librairie.
2. Sélectionner une librairie et afficher des informations de celle-ci.
3. Installer la librairie sélectionnée dans la zone des librairies disponibles [2].

## Terminal _PyCharm_

_PyCharm_ intègre un terminal permettant, entre autre, d'exécuter les programmes python en ligne de commande avec des arguments supplémentaires utilisés. Toutefois, sous Windows, _PyCharm_ utilise _powershell_ par défaut qui ne permet pas toujours de démarrer et d'utiliser un environnement virtuel _Anaconda_. De ce fait, il convient de modifier _powershell_ avec le traditionnel _cmd_ en allant dans les Préférences (_Settings_) du menu Fichier (_File_) et de modifier, dans _Tools_ --> _Terminal_, le _Shell path_ de _powershell_ à _cmd_ comme le montre la figure suivante.

![_PyCharm Terminal Settings_](img/pycharm13.png)


## Exécution et _Debugging_ avec _PyCharm_

Une fois l'environnement configuré et indexé pour un projet, un code Python (_fichier.py_) d'un projet peut être exécuté en cliquant avec le bouton droit dessus et en sélectionnant _Run "..."_ ou directement dans le menu "Run". L'exécution d'un programme python avec _PyCharm_ est très intéressante car elle permet de le [_debugger_](https://fr.wikipedia.org/wiki/D%C3%A9bogueur) et donc d'identifier plus facilement et rapidement les problèmes et surtout de les corriger. La figure suivante nous permettra d'exposer les différentes options de _debugging_ de _PyCharm_ où la zone [9] présente l'arborescence des fichiers du projet (NB: en rouge, ce sont les fichiers non suivis par _git_).

![Exécution et debugging d'un programme python](img/pycharm12.png)

Dans la liste [1], vous retrouverez l'ensemble des programmes qui ont été exécutés (_Run_) et vous pourrez également sélectionner le fichier actif courant "_current file_". Les boutons [2], [3] et [4] à côté de cette liste sont donc relatifs au fichier sélectionné avec la liste [1] et permettent respectivement d'exécuter normalement le fichier [2], de le démarrer en _debug_ [3] et finalement de l'arrêter de "manière forcée" [4]. C'est donc le bouton [3] qui va nous intéresser le plus dans la suite de cette section.

Pour qu'un démarrage avec le bouton [3] soit intéressant, il convient de mettre des _Break Points_ qui permettent de suspendre l'exécution du code à une ligne précise du code. Le placement d'un _Break Point_ s'effectue simplement en cliquant à droite du numéro de la ligne, comme le montre le point rouge [7] (_Break Point_) de la figure précédente. Pour le retirer, il suffit de recliquer dessus. Au moment de l'exécution en _debug_ avec le bouton [3], le programme s'arrête à cette ligne avant de l'exécuter et il est possible d'observer les valeurs de toutes les variables python instanciées et disponibles dans cette fonction, cf. la zone [8] de la figure précédente. Pour reprendre l'exécution normale du code jusqu'au prochain _Break Point_ (ou le même s'il est unique et exécuté à plusieurs reprises), il suffit de cliquer sur le bouton [5]. Il est aussi possible de passer à la ligne suivante avec le bouton [6] _Step Over_, les autres boutons sur le côté de [6] ont des fonctions similaires permettant par exemple d'entrer dans la fonction appelée sur la ligne courante. Notons également qu'en cas d'erreur/_crash_, l'exécution en _debug_ avec [3] permettra d'observer l'état des variables à la ligne de code provoquant cette erreur, avant que le programme s'arrête/_crash_ effectivement.


**Dernière révision** : {{ git_revision_date }}
