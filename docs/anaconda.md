# Python avec Anaconda

## Introduction

Selon [Wikipedia](https://fr.wikipedia.org/wiki/Anaconda_(Python_distribution))

> _Anaconda est une distribution libre et open source des langages de programmation Python et R appliqué au développement d'applications dédiées à la science des données et à l'apprentissage automatique (traitement de données à grande échelle, analyse prédictive, calcul scientifique), qui vise à simplifier la gestion des paquets et de déploiement. Les versions de paquetages sont gérées par le système de gestion de paquets conda. La distribution Anaconda est utilisée par plus de 6 millions d'utilisateurs et comprend plus de 250 paquets populaires en science des données adaptés pour Windows, Linux et MacOS._

Par exemple, Anaconda comprend par défaut numpy et matplotlib donnant des fonctionnalités proches de [Matlab](https://fr.mathworks.com/products/matlab.html) ainsi qu’un [`jupyter notebook`](#jupyter-notebook). 


## Installation

Il suffit d’installer le programme Anaconda compatible avec votre système d’exploitation, disponible à l’adresse [anaconda.com](https://www.anaconda.com/products/distribution), sous Windows en exécutant [l'installateur](https://repo.anaconda.com/archive/Anaconda3-2023.07-2-Windows-x86_64.exe) avec les options par défaut et [sous Ubuntu](https://repo.anaconda.com/archive/Anaconda3-2023.07-2-Linux-x86_64.sh) avec la commande `bash Anaconda3-2023.07-2-Linux-x86_64.sh`.

Si vous n’avez pas d'autre python installé, vous pouvez éventuellement le rajouter au “path”, ce qui en fera le python par défaut de votre session.

Une fois installé, vous pouvez accéder à l’environnement par défaut en utilisant le “Anaconda Prompt”.


## Console Anaconda

Dans un “Anaconda Prompt” (Console Anaconda), vous pouvez également installer des libraires supplémentaires avec les commandes `conda install` ou `pip install`. Dans la mesure du possible, veuillez installer les librairies, en priorité, avec `conda install` si elles sont disponibles.

L’“Anaconda Prompt” exécute un terminal Linux/Mac dans votre répertoire “home” ou un “command prompt” (_cmd_) Windows (ou "powershell") dans le répertoire de base de l’utilisateur (“c:\Users\Login”). Il convient donc de connaître quelques commandes de bases, par exemple `cd` pour changer de répertoire ou `ls`/`dir` pour lister les fichiers du répertoire courant. Vous trouverez la liste des commandes facilement sur internet, par exemple pour [Windows](https://www.thomas-krenn.com/en/wiki/Cmd_commands_under_Windows) et [Linux](https://www.lmd.jussieu.fr/~flott/polytechnique/mec583_08/linux_vi_f77.pdf).

L’auto-complétion est en général disponible avec la touche “Tab” de votre clavier, donc si vous souhaitez changer de répertoire, vous pouvez commencer à taper le nom du répertoire après le `cd`, la touche “Tab” complétera automatiquement la suite de son nom (plusieurs “Tab” successifs permet de voir d’autres propositions commençant par la même suite de caractères). Pour changer de disque sous Windows, il suffit de tapper la lettre du disque suivi de “:”  p/ex `d:` (Enter) pour aller sur le disque D: 

Si vous avez un fichier python “monfichier.py”, vous pouvez l’exécuter en tapant `python “monfichier.py”`.


## Travailler sous un environnement virtuel

### Introduction

Une bonne pratique est de travailler avec des environnements virtuels. Notamment afin de pouvoir séparer les dépendances (librairies) de différents projets et de collaborer plus facilement. En effet, en python, il arrive souvent qu'il y ait des différences importantes d'utilisation des fonctions d'une même librairie entre les versions. 

Dans la suite de cette section, nous allons voir comment créer un environnement sous anaconda et créer un fichier `requirements.txt` afin de pouvoir regénérer l'entièreté de nos dépendances. Même si vous ne travaillez pas dans un environnement virtuel, il est toujours intéressant d'inclure ce fichier avec votre code python pour le faire fonctionner dans les mêmes conditions que celles du développement !

**Dans le cadre du projet de cette année, l'utilisation d'un environnement virtuel n'est a priori pas nécessaire et l'environnement de "base" d'Anaconda avec l'ensemble des librairies installées par défaut sera a priori plus que suffisant !**

### Créer un environnement sous Anaconda

1. Créez votre premier environnement sous python 3.9 (bien entendu vous pouvez choisir votre version de python).
    ``shell
    conda create -n nom_environment python=3.9
    ``
2. Vérifiez que votre environnement est créé.
    ``
    conda env list 
    ``
3. Activez votre environnement afin de l'utiliser (de base, l'environnement n'est pas activé).
    ````
    conda activate nom_environment 
    ````
4. Quittez votre environnement virtuel à la fin de votre travail.
    ```
    conda deactivate
    ```
5. Supprimez votre environnement quand vous ne l'utilisez plus.
    `shell
    conda env remove -n nom_environment
    `

### Travailler avec des fichiers `requirements.txt`

Tout l'intérêt des environnements virtuels est de pouvoir reproduire un environnement de travail automatiquement et de la partager sur une autre machine.
Cela permet également de séparer les environnements de différents projets, sans tous les impacter à chaque modification.

Le fichier `requirements.txt` se place, en général, à la racine de votre projet. 

Le fichier `requirements.txt` doit lister l'entièreté de vos dépendances. Un exemple de fichier:
````text
mkdocs>=1.1.2
mkdocs-material==5.4.0
mkdocs-bootswatch
````
Remarquez les symboles `>=`, `==` qui précède le numéro de version et également les libraires sans version imposée. Dans la mesure du possible, nous conseillons toujours de fixer la version avec celle que vous avez travaillée. Si vous ne la connaissez pas, vous pouvez la trouver, par exemple, avec la commande `pip list` et/ou `conda list`.

Pour regénérer votre environnement avec ses dépendances, il vous suffit de suivre l'étape de création d'environnement virtuel, d'activer cet environnement, et d'installer les libraires avec l'utilitaire `pip` (de base fourni avec python):
````shell
python -m pip install -r requirements.txt
````

### Informations complémentaires

La documentation officielle d'Anaconda vous donne également beaucoup d'informations concernant la gestion des environnements virtuels "conda". N'hésitez pas à vous référer à [cette page](https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html) pour compléter ou éclairer le résumé ci-dessus.


## Interractive Python

Vous pouvez également exécuter un “Interactive Python” avec la commande `ipython` qui permet d’exécuter une suite de commandes python dans une console. L'autocomplétion est alors disponible avec la touche "Tab", ce qui n'est pas le cas avec une console `python`.

![IPython](img/ipython.png)


## Jupyter

L’alternative plus intéressante à `ipython` est le `jupyter notebook` ou le `jupyter lab` qui permettent de développer du code dans une interface web en utilisant des fichiers spécifiques “notebook python” dont l’extension est “ipynb”. Les commandes à exécuter dans un “Anaconda Prompt” sont respectivement `jupyter notebook` et `jupyter lab`. Ces commandes lancent alors un serveur local (sur votre PC) exécutant `jupyter`. Si votre navigateur ne se lance pas automatiquement, il suffit d’aller à l’adresse spécifiée à la fin du prompt “http://localhost:8888/?token=...” pour accéder au `jupyter notebook`. Veillez à copier/coller correctement le jeton (_token_) !

![Jupyter Start](img/jupyter_start.png)

La fenêtre de votre navigateur vous présentera alors l’arborescence des fichiers et répertoires à partir du dossier où la commande `jupyter notebook` a été exécutée. 

![Jupyter](img/jupyter.png)

Vous pouvez créer un nouveau fichier notebook python avec le bouton “New” à droite et sélectionner “Python3”, la fenêtre suivante se présentera alors.

![Jupyter New File](img/jupyter_new.png)

En cliquant sur “Untitled”, vous pouvez renommer le fichier, par exemple “test” (sous-entendu “test.ipynb” qui se situera dans le répertoire courant de votre arobrescence). Dans une cellule “Entrée” (ou “In”), vous pouvez entrer une ou plusieurs lignes de code python. Pour exécuter une cellule, il suffit de cliquer sur “Exécuter” ou plus simplement appuyer sur les touches “Shift + Enter”. A la fin de l’exécution du bloc, un chiffre incrémental apparaîtra alors autour des crochets []. Si cette cellule est encore en exécution, ce sera alors une étoile [*]. Si une sortie doit être visible pour ce bloc (print, graphique, ou autre), elle sera précédée d’un “Out [x]” avec le chiffre x correspondant au “In [x]”. En cas d’erreur, celle-ci sera également affichée en détail (voir exemple ci-dessous).

![Jupyter Error](img/jupyter_error.png)

Notons qu’il est toujours possible d’éditer une cellule pour la corriger/modifier mais il ne faut pas oublier alors de l’exécuter à nouveau. 

Le `jupyter notebook` peut également être directement présenté sous la forme d’un “jupyter lab” en mettant l’adresse suivante dans le navigateur : http://localhost:8888/lab. Rappelons que vous pouvez le lancer directement en tappant la commande `jupyter lab` dans un shell. 
Ceci permet d’avoir une interface complète et plus moderne que celle du “jupyter notebook” de base.

![Jupyter Lab](img/jupyter_lab.png)

**L’avantage principal** d’un “notebook python” (“.ipynb”) par rapport à un fichier python classique “.py” **est qu’il sauvegarde également les sorties de votre code**, ce qui vous permet de vérifier, par exemple, que les sorties correspondent toujours entre plusieurs exécutions ou de présenter vos résultats à d’autres personnes ne disposant pas nécessairement d’une installation python comme la vôtre. En outre, une sauvegarde est faite automatiquement. De ce fait, dans le cadre de ce projet, vous pouvez par exemple utliser un notebook python pour tester votre traitement sur les données qui auraient été sauvegardés oréalablement dans des fichiers. A partir de ce notebook python, vous pouvez ensuite intégrer ce code de manière plus optimale dans des fichiers python classiques.

### Utiliser Jupyter dans un environnement

Afin de pouvoir utiliser un jupyter dans un environnement, le plus simple est de l'installer dans cet environnement :

1. Activez l'environnement virtuel que vous souhaitez ajouter à jupyter (voir [plus haut](#creer-un-environnement-sous-anaconda)).
2. Installez ipykernel,([interactive python shell](https://ipython.readthedocs.io/en/stable/index.html)), jupyter et jupyterlab.
    ````shell
    conda install -c anaconda ipykernel jupyter jupyterlab
    ````

Au moment de la création d'un environnement, il est également possible d'installer tout les packages anaconda présent par défaut dont jupyter, numpy, .. avec la commande suivante :
    ````shell
    conda create -n nom_environment python=3.9 anaconda
    ````


## Structure d'un projet en python (facultatif)

Il n'existe aucune contrainte par rapport à la structure d'un projet en python. Mais il est bon d'avoir de bonnes pratiques au niveau de la structure d'un projet même si, dans le cadre de ce projet, une structure de base vous soit proposée et donnée par le biais du template du projet "[IRCam](https://gitlab.com/ulb-polytech/ba2-projects/biomed/2023-2024/ircam)" intégrant l'ensemble de vos documents, vos codes arduino, vos codes python dans le dossier `src` et vos fichiers de gestion de projet. 

Voici un exemple de projet classique. Les éléments tagués `optionnel` ne devraient pas vous être utiles sauf si vous décidez d'aller plus loin (tel que créer un package de votre application).

![Jupyter New File](img/project_example.jpg)

1. `src` : Le dossier contenant le code de votre projet (généralement les fichiers `.py`).
2. `docs`: La documentation de votre projet (en MarkDown).
3. `tests`(**optionnel**): Un dossier contenant l'ensemble des tests unitaires /tests de votre code (s'ils sont nécessaires).
4. `.env`: Fichier contenant des variables sensibles tel que des mots de passe (**Faire attention à ne jamais mettre sur git**, l'ajouter donc au fichier [`.gitignore`](https://git-scm.com/docs/gitignore)).
5. `.gitignore`: Fichier contenant l'ensemble des fichiers à na pas mettre sur un repertoire distance (**c'est ici que vous listez les fichiers `.env*` ** afin de ne jamais les mettre sur un répertoire distant). Dans tous les cas, il devra toujours se situer à la racine de votre repositoire. 
6. `CHANGELOG.md` (**optionnel**): Si vous décidez de faire différentes versions de votre application, le `CHANGELOG` permet de garder une trace des modifications majeurs/mineures apportées.
7. `LICENSE` (**optionnel**): Protection juridique.
8. `README.md`: La page de documentation s'affichant par défaut lorsque vous être dans un répertoire sur github ou gitlab. Vous pouvez avoir un README dans chaque dossier et écrire une petite documentation à l'intérieur pour donner une explication du dossier en cours.
9. `requirements.txt`: La liste de vos dépendances en terme de librairie, à mettre à jour régulièrement au cours du projet. Notons que pour des raisons pratiques, ce fichier a été placé dans le répertoire `src` du template du projet de cette année.
10. `setup.py` (**optionnel**): Un fichier de setup qui va vous permettre de créer une distribution (un package) à partir de votre code.

Pour plus de détails:

- [Documentation](https://docs.python-guide.org/writing/documentation/)
- [Structure](https://docs.python-guide.org/writing/structure/)
- [Tests](https://docs.python-guide.org/writing/tests/#py-test)
- [dotenv](https://github.com/theskumar/python-dotenv)
- [requirements](https://pip.pypa.io/en/stable/user_guide/#requirements-files)
- [command-line-interface (cli)](https://click.palletsprojects.com/en/8.0.x/)


**Dernière révision** : {{ git_revision_date }}
